class IllegalArgumentError extends Error {

  constructor(message) {
    super("Illegal argument: " + message);
    Error.captureStackTrace(this, IllegalArgumentError);
  }
}
module.exports = {IllegalArgumentError}


