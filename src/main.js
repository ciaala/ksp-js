const list_ksp = require('./ksp-game-mods.js').list_ksp;
const searchSpaceDock = require('./api-space-dock').searchSpaceDock;
function spaceDock (conf, key, value) {
  console.log(key + ' from "' + value + '"');
  searchSpaceDock(conf.space_dock.url, key.substring(0, key.length-4))
  .then(data => console.log(data))
  .catch(error=> console.error(error));
}

async function main() {
  const conf = {
    ksp: {'dir': '/home/crypt/applications/Steam/steamapps/common/Kerbal Space Program/GameData/'},
    space_dock: {url: 'https://spacedock.info/api'},
  };
  const modules = await list_ksp(conf.ksp);
  // console.log(typeof modules);
  // console.log(modules.keys());
  modules.forEach(
      (value, key) => {
        spaceDock(conf, key, value)
      }
  );
}

main();
