const fs = require('fs').promises;
const pathHelper = require('path');

async function ksp_print(startPath, consumer) {
  const directoriesQueue = [startPath];
  while (directoriesQueue.length > 0) {
    const path = directoriesQueue.pop();
    const dir = await fs.opendir(path);
    for await (const dirEntry of dir) {
      if (dirEntry.isDirectory()) {
        const directory = pathHelper.join(path, dirEntry.name);
        directoriesQueue.push(directory);
      } else if (dirEntry.isFile() && dirEntry.name.endsWith('dll')) {
        const moduleDll = pathHelper.join(path, dirEntry.name);
        consumer({ name : dirEntry.name, path : moduleDll});
      }
    }
  }
}

/**
 *
 * @param {dir{string}} the directory where kerbal space program is installed
 * @returns {Promise<Map<string, string>>}
 */
async function list_ksp(conf) {
  console.log('List all the KSP modules');
  const modules = new Map();
  await ksp_print(conf.dir, o => {
    modules.set(o.name, o.path);
  }).catch(console.error);

  return modules;
}


module.exports = {list_ksp};
