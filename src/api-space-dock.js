const fetch = require('node-fetch');
const IllegalArgumentError = require('./exception.js').IllegalArgumentError;

function browse(conf) {
  fetch(conf.url + '/browse?count=1200&orderBy=name').
  then(res => res.json()).
  then(json => {
        const object = new Map();
        const result = json.result;
        for (let i = 0; i < result.length; i++) {
          const key = result[i].name;
          object[key] = result[i].versions[0];
        }
        console.log(object);
      },
  );
}

class StringUtils {
  static isBlank(value) {
    return !value || String(value).trim().length === 0;
  }
}

/**
 *
 * @param url {string} the url of the service
 * @param kspModuleName {string} the name of the KSP's mod
 * @returns
 */
async function searchSpaceDock (url, kerbalModuleName) {
  if (StringUtils.isBlank(url) || StringUtils.isBlank(kerbalModuleName)) {
    throw new IllegalArgumentError(
        'both url and moduleName must not be valid strings');
  }
  console.log('SEARCHING:' + kerbalModuleName);
  const searchUrl = url + '/search/mod?query=' + kerbalModuleName;
  console.log('SpaceDock Search Url: ' + searchUrl);

  const response = await fetch(searchUrl);
  const json = await response.json();

  const object = new Map();
  const result = json;
  for (let i = 0; i < result.length; i++) {
    const matchName = result[i].name;
    console.log('checking "' + matchName + '"')
    if (matchName.indexOf(kerbalModuleName) > -1) {
      console.log('Matches', [kerbalModuleName, matchName]);
      const version = result[i].versions[0];
      if (version && version.changelog) {
        version.changelog = version.changelog.substr(0, 64);
      }
      // it is using Map Syntax
      // should not use the syntax [] from object
      object.set(matchName, result[i].versions[0]);
    }
  }
  console.log('SEARCHING results: ', object);
  return object;
}

function list_space_dock(conf) {
  search(conf);
}

module.exports = {
  searchSpaceDock: searchSpaceDock,
};
