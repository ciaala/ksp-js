const apiSpaceDock = require('../src/api-space-dock');

const BE_VALID_STRINGS = 'Illegal argument: both url and moduleName must not be valid strings';
test('Testing invalid input of searchSpaceDock',
    () => {
      //const result = apiSpaceDock.searchSpaceDock(null,null);
      const invalidValues = [null, undefined, '', '  '];
      for (const firstInvalidValue of invalidValues) {
        for (const secondInvalidValue of invalidValues) {
          expect(async () =>
              await apiSpaceDock.searchSpaceDock(firstInvalidValue,
                  secondInvalidValue).rejects.toThrowError(BE_VALID_STRINGS));
        }
      }
      for (const invalidValue of invalidValues) {
        expect(async () =>
            await apiSpaceDock.searchSpaceDock('http://aol.com',
                invalidValue).rejects.toThrowError(BE_VALID_STRINGS));
      }
      for (const invalidValue of invalidValues) {
        expect(async () =>
            await apiSpaceDock.searchSpaceDock(invalidValue, 'MechJeb').
            rejects.toThrowError(BE_VALID_STRINGS));
      }
    },
);

test('Searching for valid module',
    async () => {
      const result = apiSpaceDock.searchSpaceDock(

          'https://spacedock.info/api',
          'MechJeb');
      await result
        .then( data => {
          expect(data).toBeInstanceOf(Map)
          expect(data.size).toEqual(1);
          const iterator = data.entries();
          const item = iterator.next().value;
          expect(item[0]).toContain('MechJeb')
          expect(item[1]['download_path']).toBeDefined();
          console.log(data);
        })
        .catch(error => expect(error).not.toBeDefined())
    },
);

